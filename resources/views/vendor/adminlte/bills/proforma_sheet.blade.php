<!DOCTYPE html>
<html>
	<body>
		<table style="width: 40%;text-align: center;color: #000000;">
			<!-- A1 -->
			<tr>
				<td colspan="7" style="width: 7;"></td>
			</tr>
			<!-- A2 -->
			<tr>
				<td colspan="7" style="width: 7;"></td>
			</tr>
			<!-- A3 -->
			<tr>
				<td colspan="7" style="width: 7;"></td>
			</tr>
			<!-- A4 -->
			<tr>
				<td colspan="7" style="text-align: center;font-size: 18;"><b>Tax Invoice</b></td>
			</tr>
			<!-- A5 -->
			<tr>
				<td colspan="7" style="width: 7;"></td>
			</tr>
			<!-- A6 -->
			<tr style="">
				<td style="border:1px solid #000000;height: 40;width: 10;vertical-align: top;">{!! $invoice_data['address'] !!}</td>
				<td colspan="2" style="border:1px solid #000000;height: 40;vertical-align: top;"></td>
				<td colspan="2" style="border:1px solid #000000;vertical-align: top;">Invoice No. <br/> PROFORMA</td>
				<td colspan="2" style="border:1px solid #000000;vertical-align: top;">{!! $invoice_data['dated'] !!}</td>
			</tr>
			<!-- A7 -->
			<tr style="">
				<td style="border:1px solid #000000;height: 40;vertical-align: top;"></td>
				<td colspan="2" style="border:1px solid #000000;height: 40;vertical-align: top;"></td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 25;vertical-align: top;">Delivery Note</td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 25;vertical-align: top;">Mode/Terms of Payment</td>
			</tr>
			<!-- A8 -->
			<tr style="">
				<td style="border:1px solid #000000;height: 40;"></td>
				<td colspan="2" style="border:1px solid #000000;height: 40;vertical-align: top;"></td>
				<td colspan="2" style="border:1px solid #000000;vertical-align: top;">Supplier's Ref. <br/> PROFORMA</td>
				<td colspan="2" style="border:1px solid #000000;vertical-align: top;">Other Reference(s)</td>
			</tr>
			<!-- A9 -->
			<tr style="">
				<td style="border:1px solid #000000;height: 40;width: 10;vertical-align: top;">{!! $invoice_data['billing_address'] !!}</td>
				<td colspan="2" style="border:1px solid #000000;height: 40;vertical-align: top;"></td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 25;vertical-align: top;">Buyer's Order No.</td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 25;vertical-align: top;">Dated</td>
			</tr>
			<!-- A10 -->
			<tr style="">
				<td style="border:1px solid #000000;height: 40;"></td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 13;vertical-align: top;"></td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 25;vertical-align: top;">Despatch Document No.</td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 25;vertical-align: top;">Delivery Note Date</td>
			</tr>
			<!-- A11 -->
			<tr style="">
				<td style="border:1px solid #000000;height: 40;"></td>
				<td colspan="2" style="border:1px solid #000000;height: 40;vertical-align: top;"></td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 25;vertical-align: top;">Despatched through</td>
				<td colspan="2" style="border:1px solid #000000;height: 40;width: 25;vertical-align: top;">Destination</td>
			</tr>
			<!-- A12 -->
			<tr style="">
				<td colspan="3" style="border:1px solid #000000;height: 40;"></td>
				<td colspan="4" style="border:1px solid #000000;height: 40;vertical-align: top;">Terms of Delivery</td>
			</tr>
			<!-- A13 -->
			<tr style="text-align: center;">
				<td style="border:1px solid #000000;width: 9;">Sl <br/> No.</td>
				<td style="border:1px solid #000000;width: 10;text-align: center;">Particulars</td>
				<td style="border:1px solid #000000;">HSN/SAC</td>
				<td style="border:1px solid #000000;">Quantity</td>
				<td style="border:1px solid #000000;">Rate</td>
				<td style="border:1px solid #000000;width: 8;">per</td>
				<td style="border:1px solid #000000;width: 13;">Amount</td>
			</tr>
			<!-- A14 -->
			<tr style="border-bottom: none;">
				<td style="border:1px solid #000000;">1</td>
				<td style="border:1px solid #000000;height: 60;width: 36;border-bottom: none;vertical-align: middle;text-align: center;">{!! $invoice_data['placement_service'] !!}</td>
				<td style="border:1px solid #000000;vertical-align: top;">998512</td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['fees'] }}</td>
			</tr>

			@if(isset($invoice_data['gst_check']) && $invoice_data['gst_check'] == '24')
				<!-- A15 -->
				<tr style="">
					<td style="border:1px solid #000000;">2</td>
					<td style="border:1px solid #000000;text-align: right;">CGST</td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['cgst'] }}</td>
				</tr>
				<!-- A16 -->
				<tr style="">
					<td style="border:1px solid #000000;">3</td>
					<td style="border:1px solid #000000;text-align: right;">SGST</td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['sgst'] }}</td>
				</tr>
			@else
				<!-- A15 -->
				<tr style="">
					<td style="border:1px solid #000000;">2</td>
					<td style="border:1px solid #000000;">IGST</td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['igst'] }}</td>
				</tr>
			@endif
			<!-- A17 -->
			<tr style="">
				<td style="border:1px solid #000000;">4</td>
				<td style="border:1px solid #000000;">Less : ROUND OFF</td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;text-align: right;">(-){{ $invoice_data['less_amount'] }}0</td>
			</tr>
			<!-- A18 -->
			<tr style="">
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;text-align: right;">Total</td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;"></td>
				<td style="border:1px solid #000000;text-align: right;">₹ {{ $invoice_data['billing_amount'] }}</td>
			</tr>
			<!-- A19 -->
			<tr style="">
				<td colspan="6" style="border:1px solid #000000;">Amount Chargeable (in words)</td>
				<td style="border:1px solid #000000;">E. & O.E</td>
			</tr>
			<!-- A20 -->
			<tr style="">
				<td colspan="7" style="border:1px solid #000000;">{{ $invoice_data['amount_in_words'] }} Only</td>
			</tr>

			@if(isset($invoice_data['gst_check']) && $invoice_data['gst_check'] == '24')
				<!-- A21 -->
				<tr style="">
					<td rowspan="2" style="border:1px solid #000000;text-align: center;">HSN/SAC</td>
					<td style="border:1px solid #000000;text-align: center;">Taxable <br/> Value</td>
					<td colspan="2" style="border:1px solid #000000;text-align: center;">Central Tax</td>
					<td colspan="2" style="border:1px solid #000000;text-align: center;">State Tax</td>
					<td style="border:1px solid #000000;text-align: center;">Total <br/> Tax Amount</td>
				</tr>
				<!-- A22 -->
				<tr style="">
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;text-align: center;">Rate</td>
					<td style="border:1px solid #000000;text-align: center;">Amount</td>
					<td style="border:1px solid #000000;text-align: center;">Rate</td>
					<td style="border:1px solid #000000;text-align: center;">Amount</td>
					<td style="border:1px solid #000000;"></td>
				</tr>
				<!-- A23 -->
				<tr style="">
					<td style="border:1px solid #000000;">998512</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['fees'] }}</td>
					<td style="border:1px solid #000000;text-align: right;">9%</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['cgst'] }}</td>
					<td style="border:1px solid #000000;text-align: right;">9%</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['sgst'] }}</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['total_tax_amount'] }}</td>
				</tr>
				<!-- A24 -->
				<tr style="">
					<td style="border:1px solid #000000;text-align: right;">Total</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['fees'] }}</td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['cgst'] }}</td>
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['sgst'] }}</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['total_tax_amount'] }}</td>
				</tr>
			@else
				<!-- A21 -->
				<tr style="">
					<td rowspan="2" style="border:1px solid #000000;text-align: center;">HSN/SAC</td>
					<td rowspan="2" style="border:1px solid #000000;">Taxable <br/> Value</td>
					<td colspan="3" style="border:1px solid #000000;text-align: center;">Integrated Tax</td>
					<td rowspan="2" style="border:1px solid #000000;">Total <br/> Tax Amount</td>
				</tr>
				<!-- A22 -->
				<tr style="">
					<td style="border:1px solid #000000;"></td>
					<td style="border:1px solid #000000;"></td>
					<td colspan="2" style="border:1px solid #000000;text-align: center;">Rate</td>
					<td colspan="2" style="border:1px solid #000000;text-align: center;">Amount</td>
					<td style="border:1px solid #000000;"></td>
				</tr>
				<!-- A23 -->
				<tr style="">
					<td style="border:1px solid #000000;">998512</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['fees'] }}</td>
					<td colspan="2" style="border:1px solid #000000;text-align: right;">18%</td>
					<td colspan="2" style="border:1px solid #000000;text-align: right;">{{ $invoice_data['igst'] }}</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['igst'] }}</td>
				</tr>
				<!-- A24 -->
				<tr style="">
					<td style="border:1px solid #000000;text-align: right;">Total</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['fees'] }}</td>
					<td colspan="2" style="border:1px solid #000000;text-align: right;"></td>
					<td colspan="2" style="border:1px solid #000000;text-align: right;">{{ $invoice_data['igst'] }}</td>
					<td style="border:1px solid #000000;text-align: right;">{{ $invoice_data['igst'] }}</td>
				</tr>
			@endif
			<!-- A25 -->
			<tr style="">
				<td colspan="7" style="border:1px solid #000000;">Tax Amount (in words) : {{ $invoice_data['tax_amount_word'] }} Only</td>
			</tr>
			<!-- A26 -->
			<tr style="">
				<td colspan="7" style="border:1px solid #000000;"></td>
			</tr>
			<!-- A27 -->
			<tr style="">
				<td colspan="3" style="border:1px solid #000000;"></td>
				<td colspan="4" style="border:1px solid #000000;">Company's Bank Details</td>
			</tr>
			<!-- A28 -->
			<tr style="">
				<td colspan="3" style="border:1px solid #000000;"></td>
				<td colspan="4" style="border:1px solid #000000;">Bank Name : ICICI BANK</td>
			</tr>
			<!-- A29 -->
			<tr style="">
				<td colspan="3" style="border:1px solid #000000;"></td>
				<td colspan="4" style="border:1px solid #000000;">A/c No. : 006705006549</td>
			</tr>
			<!-- A30 -->
			<tr style="">
				<td colspan="3" style="border:1px solid #000000;">Company's PAN : AAQCA2473C</td>
				<td colspan="4" style="border:1px solid #000000;">Branch & IFS Code: JODHPUR GAM & ICIC0000067</td>
			</tr>
			<!-- A31 -->
			<tr style="">
				<td colspan="3" style="border:1px solid #000000;"></td>
				<td colspan="4" style="border:1px solid #000000;">for AMANI CORPORATE SOLUTIONS PVT. LTD.</td>
			</tr>
			<!-- A32 -->
			<tr style="">
				<td colspan="7" style="border:1px solid #000000;"></td>
			</tr>
			<!-- A33 -->
			<tr style="">
				<td colspan="3" style="border:1px solid #000000;"></td>
				<td colspan="4" style="border:1px solid #000000;">Authorised Signatory</td>
			</tr>
			<!-- A34 -->
			<tr>
				<td colspan="7" style=""></td>
			</tr>
			<!-- A35 -->
			<tr>
				<td colspan="7" style="text-align: center;">SUBJECT TO AHMEDABAD JURISDICTION</td>
			</tr>
			<!-- A36 -->
			<tr>
				<td colspan="7" style="text-align: center;font-size: 10">This is a Computer Generated Invoice</td>
			</tr>
		</table>
	</body>
</html>