@extends('adminlte::page')

@section('title', 'User')

@section('content_header')
    <h1>Edit User Details</h1>
@stop

@section('customs_css')
    <style>
        .error{
            color:#f56954 !important;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
        </div>
    </div>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    </div>
@endif

{!! Form::model($user, ['method' => 'PATCH','id' => 'userform','route' => ['users.update', $user->id]]) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="box box-warning col-xs-12 col-sm-12 col-md-12">
                <div class="box-header col-md-6 ">

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="box-body col-xs-12 col-sm-12 col-md-12">
                        <div class="">

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <strong>Name: <span class = "required_fields">*</span> </strong>
                                {!! Form::text('name', null, array('id'=>'name','placeholder' => 'Name','class' => 'form-control', 'tabindex' => '1' )) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <strong>Email: <span class = "required_fields">*</span> </strong>
                                {!! Form::email('email', null, array('id'=>'email','placeholder' => 'Email','class' => 'form-control', 'tabindex' => '2' )) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>


                            <div class="form-group {{ $errors->has('semail') ? 'has-error' : '' }}">
                                <strong>Secondary Gmail: </strong>
                                {!! Form::email('semail', $semail, array('id'=>'semail','placeholder' => 'Secondary Gmail','class' => 'form-control', 'tabindex' => '3' )) !!}
                                @if ($errors->has('semail'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('semail') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <strong>Password: <span class = "required_fields">*</span> </strong>
                                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control','tabindex' => '4' )) !!}
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('confirm-password') ? 'has-error' : '' }}">
                                <strong>Confirm Password: <span class = "required_fields">*</span></strong>
                                {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control','tabindex' => '5' )) !!}
                                @if ($errors->has('confirm-password'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('confirm-password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('reports_to') ? 'has-error' : '' }}">
                                <strong>Reports To : </strong>
                                {!! Form::select('reports_to', $reports_to,isset($userReportsTo) ? $userReportsTo : null, array('id'=>'reports_to','class' => 'form-control','tabindex' => '6')) !!}
                                @if ($errors->has('reports_to'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reports_to') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('company_id') ? 'has-error' : '' }}">
                                <strong>Select Company : <span class = "required_fields">*</span> </strong>
                                {!! Form::select('company_id', $companies,
                                isset($user->compnay_id) ? $user->compnay_id : null, array('id'=>'company_id','class' => 'form-control','tabindex' => '7')) !!}
                                @if ($errors->has('company_id'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('company_id') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                                <strong> Role : <span class = "required_fields">*</span> </strong>
                                {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','tabindex' => '8','id' => 'roles')) !!}
                                @if ($errors->has('roles'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('roles') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                                <strong> Employee Type : <span class = "required_fields">*</span> </strong>
                                {!! Form::select('type', $type,null, array('class' => 'form-control','tabindex' => '9','id' => 'type')) !!}
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <strong> Generate Report : </strong> &nbsp;&nbsp;
                                {!! Form::radio('daily_report','Yes', true) !!}
                                {!! Form::label('Yes') !!} &nbsp;&nbsp;
                                {!! Form::radio('daily_report','No') !!}
                                {!! Form::label('No') !!}
                            </div>
                          
                            <div class="form-group">
                                <strong> Status : </strong> &nbsp;&nbsp;
                                {!! Form::radio('status','Active') !!}
                                {!! Form::label('Active') !!} &nbsp;&nbsp;
                                {!! Form::radio('status','Inactive') !!}
                                {!! Form::label('Inactive') !!}
                            </div>

                            <div class="form-group">
                                <strong> Account Manager : </strong> &nbsp;&nbsp;
                                {!! Form::radio('account_manager','Yes') !!}
                                {!! Form::label('Yes') !!} &nbsp;&nbsp;
                                {!! Form::radio('account_manager','No') !!}
                                {!! Form::label('No') !!}
                            </div>

                            <div class="form-group">
                                <strong> Franchise Owner : </strong> &nbsp;&nbsp;
                                {!! Form::radio('franchise_owner','1') !!}
                                {!! Form::label('Yes') !!} &nbsp;&nbsp;
                                {!! Form::radio('franchise_owner','0') !!}
                                {!! Form::label('No') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>

    {!! Form::close() !!}
@endsection

@section('customscripts')
<script>
    $(document).ready(function()
    {
        $("#reports_to").select2();
        $("#company_id").select2();
        $("#roles").select2();
        $("#type").select2();

        $("#userform").validate(
        {
            rules: {
                "name": {
                    required: true
                },
                "email": {
                    required: true
                },
                "password": {
                    required: true
                },
                "confirm-password": {
                    required: true
                },
            
                "company_id": {
                    required: true
                },
                "roles": {
                    required: true
                },
                "type": {
                    required: true
                }
            },
            messages: {
                "name": {
                    required: "Name is Requird Field."
                },
                "email": {
                    required: "Email is Requird Field."
                },
                "password": {
                    required: "Password is Requird Field."
                },
                "confirm-password": {
                    required: "Confirm Password is Requird Field."
                },
                "company_id": {
                    required: "Please Select Company."
                },
                "roles": {
                    required: "Please Select User role."
                },
                "type": {
                    required: "Please Select type."
                }
            },
        });
    });
</script>
@endsection