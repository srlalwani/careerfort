<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Carrer Fort</title>

    @yield('style')
</head>

<body style="margin: 0; padding-top: 30px; background-color: #f5f5f5;">

<body>
<table border="0" cellpadding="0" cellspacing="0" width="600" style="font-family:Helvetica,Arial,sans-serif; border-collapse: collapse; color: #444444;" align="center">
    <tr>
        <td width="600" >
            <table width="100%" cellpadding="0" cellspacing="0" style="border:0;height: 70px;">
                <tr style="background-color:white;">
                    <td colspan="2"></td>
                    <td align="center">
                        <div class="site-branding col-md-2 col-sm-6 col-xs-12" >
                            <span style="font-size: 22px;text-decoration:none">
                                <img width="600" class="site-logo" src="{{$app_url}}/images/career_logo.jpg" alt="Career Fort" style="height: 90px;padding-top: 16px; vertical-align: middle;">
                            </a>
                        </div>
                    </td>
                    <td colspan="2"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="600"  style="background-color: green; !important;">
            <table width="100%" cellpadding="0" cellspacing="0" style="border:0; background-color: #ffffff; padding: 50px 54px;">
                <tr>
                    <td>
                        <b><p style="margin-top: 0px; margin-bottom: 14px; font-family: arial;">Dear {{$cname}},</p></b>
                        <i><p>Greetings from Career Fort !! </p></i>
                        <p style="text-align: justify;">
                            Career Fort Consultancy (A Unit of Amani Corporate Solutions Pvt Ltd) is a professional recruitment services consultancy committed to delivering the very best in the areas of consultancy. We are providing one stop consultancy solutions & services.We are in - HR recruitment Consulting, Outsourcing, Payroll Management, IT Solutions, Legal - PF, ESIC, Factory Act etc., Training & Development, Industrial Labour and House-keeping, Security, NEEM Agent etc.
                        </p>
                        <p><u> As per our telephonic conversation please find the interview details below:</u></p>
                        <p><b>Company Name : </b>{{ $company_name }}</p>
                        <p><b>Company URL: </b><a href="{{ $company_url }}">{{ $company_url }}</a></p>
                        <p><b>About Client : </b></p>
                        <p>{!! $client_desc !!}</p>
                        <p><b>Job Designation :</b> {{ $job_designation }}</p>
                        <p><b>Job Location :</b> {{ $job_location }}</p>
                        <p><b>Job Description :</b></p>
                        <p> {!! $job_description !!}</p>
                        <p><b>Interview Date/Day : </b> {{date('jS F,y (l)',strtotime($interview_date)) }}</p>
                        <p><b>Interview Time : </b> {{date('h:i A',strtotime($interview_time))  }}</p>
                        <p><b>Interview Venue : </b> {{ $interview_location }}</p>
                        <p><b>Contact Person : </b>{{$contact_person}}</p>
                        <p style="color:red;"><u>Please carry a copy of your updated resume at the time of interview.<br/>Request you to acknowledge the receipt of this mail.</u></p>
                        <p>For any query/discussion, feel free to connect with me anytime.</p>
                        <p>Thanks</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="600" >
            <table width="100%" cellpadding="0" cellspacing="0" style="border:0;height: 70px;">
                <tr style="height: 45px; background-color: #dddddd;">
                    <td style="text-align: center; font-size: 11px; color: #888888; font-family: arial;">Copyright Career fort <?php echo date('Y'); ?>. All rights reserved</td>
                </tr>
            </table>
        </td>
    </tr>
</table>



</body>
</html>