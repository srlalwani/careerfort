@extends('adminlte::page')

@section('title', 'Lead Details')

@section('content_header')
    <h1></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>{{ $lead->name }}</h2>
            </div>

            <div class="pull-right">
                 <a class="btn btn-primary" href="{{ route('lead.index') }}">Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="box box-warning col-xs-12 col-sm-12 col-md-12">
                <div class="box-header col-md-6 ">
                    <h3 class="box-title"></h3>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <table class="table table-bordered">
                        <tr>
                           <th>Contact Point </th>
                           <td>{{ $lead->coordinator_name }}</td>
                           <th>Display Name </th>
                           <td>{{ $lead->display_name }}</td>
                        </tr>
                        <tr>
                           <th>Email </th>
                           <td>{{ $lead->mail }}</td>
                           <th>Secondary Email</th>
                           <td>{{ $lead->s_email }}</td>
                        </tr>
                        <tr>
                           <th>Phone Number </th>
                           <td>{{ $lead->mobile }}</td>
                           <th>Other Number </th>
                           <td>{{ $lead->other_number }}</td>
                        </tr>
                        <tr>
                           <th>Lead Service </th>
                           <td>{{ $lead->service }}</td>
                           <th>Lead Status </th>
                           <td>{{ $lead->lead_status }}</td>
                        </tr>
                        <tr>
                           <th>Source </th>
                           <td>{{ $lead->source }}</td>
                           <th>Designation </th>
                           <td>{{ $lead->designation }}</td>
                        </tr>
                        <tr>
                           <th>Website </th>
                           <td>{{ $lead->website }}</td>
                           <th>Referred By </th>
                           <td>{{ $referredby }}</td>
                        </tr>
                        <tr>
                           <th>Remarks </th>
                           <td>{{ $lead->remarks }}</td>
                           <th>Country</th>
                           <td>{{ $lead->country }}</td> 
                        </tr>
                        <tr>
                           <th>State</th>
                           <td>{{ $lead->state }}</td>
                           <th>City</th>
                           <td>{{ $lead->city }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

