<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillsDoc extends Model
{
    public $table = "bills_doc";
    public $timestamps = false;
}
