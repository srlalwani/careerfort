<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Utils;
use Illuminate\Http\Request;
use App\ClientBasicinfo;
use App\ClientAddress;
use App\ClientDoc;
use App\Industry;
use Illuminate\Support\Facades\Input;
use Mockery\CountValidator\Exception;
use Storage;
use App\User;
use App\JobOpen;
use App\Lead;
use Excel;
use App\Events\NotificationEvent;
use App\Events\NotificationMail;
use App\EmailsNotifications;
use App\JobVisibleUsers;
use App\Post;

class ClientController extends Controller
{
    public function index(Request $request){
        $utils = new Utils();
        $user =  \Auth::user();

        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isAccountant = $user_obj::isAccountant($role_id);
        $isManager = $user_obj::isManager($role_id);
        $isAccountManager = $user_obj::isAccountManager($user->id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isBDE || $isManager /*|| $isAdmin || $isAccountant*/){
            $client_array = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
            $count = sizeof($client_array);
        }
        else{
            $client_array = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
            $count = sizeof($client_array);
        }

        $account_manager=User::getAllUsers('recruiter');

        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($client_array as $client){
            
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount')
            {
                $para_cat++;
            }
            else if($client['category'] == 'Moderate')
            {
                $mode_cat++;
            }
            else if($client['category'] == 'Standard')
            {
                $std_cat++;
            }
        }

        return view('adminlte::client.index',compact('client_array','isAdmin','isSuperAdmin','count','active','passive','isBDE','isAccountManager','account_manager','para_cat','mode_cat','std_cat','leaders','forbid','left'));
    }

    public static function getOrderColumnName($order,$admin){
        $order_column_name = '';
        if($admin){
            if (isset($order) && $order >= 0) {
                if ($order == 1) {
                    $order_column_name = "client_basicinfo.id";
                }
                else if ($order == 2) {
                    $order_column_name = "users.name";
                }
                else if ($order == 3) {
                    $order_column_name = "client_basicinfo.name";
                }
                else if ($order == 4) {
                    $order_column_name = "client_basicinfo.coordinator_prefix";
                }
                else if ($order == 5) {
                    $order_column_name = "client_basicinfo.category";
                }
                else if ($order == 6) {
                    $order_column_name = "client_basicinfo.status";
                }
                else if ($order == 7) {
                    $order_column_name = "client_address.billing_street2";
                }
            }
        }
        else{
            if (isset($order) && $order >= 0) {
                if ($order == 1) {
                    $order_column_name = "client_basicinfo.id";
                }
                else if ($order == 2) {
                    $order_column_name = "users.name";
                }
                else if ($order == 3) {
                    $order_column_name = "client_basicinfo.name";
                }
                else if ($order == 4) {
                    $order_column_name = "client_basicinfo.coordinator_prefix";
                }
                else if ($order == 5) {
                    $order_column_name = "client_basicinfo.status";
                }
                else if ($order == 6) {
                    $order_column_name = "client_address.billing_street2";
                }
            }
        }
        return $order_column_name;
    }

    public function getAllClientsDetails(){

        $draw = $_GET['draw'];
        $limit = $_GET['length'];
        $offset = $_GET['start'];
        $search = $_GET['search']['value'];
        $order = $_GET['order'][0]['column'];
        $type = $_GET['order'][0]['dir'];

        $user =  \Auth::user();
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isAccountant = $user_obj::isAccountant($role_id);
        $isManager = $user_obj::isManager($role_id);
        $isAccountManager = $user_obj::isAccountManager($user->id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE/*|| $isAccountant*/){
            $order_column_name = self::getOrderColumnName($order,1);
            $client_res = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions,$limit,$offset,$search,$order_column_name,$type);
            $count = ClientBasicinfo::getAllClientsCount(1,$user->id,$search);
        }
        else if($isAccountManager){
            $order_column_name = self::getOrderColumnName($order,1);
            $client_res = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions,$limit,$offset,$search,$order_column_name,$type);
            $count = ClientBasicinfo::getAllClientsCount(0,$user->id,$search);
        }
        else{
            $order_column_name = self::getOrderColumnName($order,0);
            $client_res = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions,$limit,$offset,$search,$order_column_name,$type);
            $count = ClientBasicinfo::getAllClientsCount(0,$user->id,$search);
        }
        //print_r($client_array);exit;
        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $clients = array();
        $i = 0;
        foreach ($client_res as $key => $value) {
            $action = '';
            //if($isSuperAdmin || $isAdmin || $isBDE || $value['client_visibility'] || $isAccountant){
                $action .= '<a title="Show" class="fa fa-circle"  href="'.route('client.show',$value['id']).'" style="margin:2px;"></a>'; 
            //}
            if($isSuperAdmin || $isManager || $value['client_owner'] || $isBDE){
                $action .= '<a title="Edit" class="fa fa-edit" href="'.route('client.edit',$value['id']).'" style="margin:2px;"></a>';
            }
            if($isSuperAdmin){
                $delete_view = \View::make('adminlte::partials.deleteModalNew', ['data' => $value, 'name' => 'client','display_name'=>'Client']);
                $delete = $delete_view->render();
                $action .= $delete;
                if(isset($value['url']) && $value['url']!=''){
                    $action .= '<a target="_blank" href="'.$value['url'].'"><i  class="fa fa-fw fa-download"></i></a>';
                }
            }
            if($isSuperAdmin || $isBDE || $isManager){
                $account_manager_view = \View::make('adminlte::partials.client_account_manager', ['data' => $value, 'name' => 'client','display_name'=>'More Information', 'account_manager' => $account_manager]);
                $account = $account_manager_view->render();
                $action .= $account;
            }
            if($isSuperAdmin || $value['client_owner']){
                $action .= '<a title="Remarks" class="fa fa-plus"  href="'.route('client.remarks',$value['id']).'" style="margin:2px;"></a>';
            }

            $checkbox = '<input type=checkbox name=client value='.$value['id'].' class=others_client id='.$value['id'].'/>';
            $company_name = '<a style="white-space: pre-wrap; word-wrap: break-word; color:black; text-decoration:none;">'.$value['name'].'</a>';
            if($isSuperAdmin || $isBDE/* || $isAccountManager*/ ){
                $client_category = $value['category'];
            }
            if($value['status']=='Active')
                $client_status = '<span class="label label-sm label-success">'.$value['status'].'</span></td>';
            else if($value['status']=='Passive')
                $client_status = '<span class="label label-sm label-danger">'.$value['status'].'</span></td>';
            else if($value['status']=='Leaders')
                $client_status = '<span class="label label-sm label-primary">'.$value['status'].'</span></td>';
            else if($value['status']=='Forbid')
                $client_status = '<span class="label label-sm label-default">'.$value['status'].'</span>';
            else if($value['status']=='Left')
                $client_status = '<span class="label label-sm label-info">'.$value['status'].'</span>';
            if($isSuperAdmin || $isBDE /*|| $isAccountManager*/ ){
                //$data = array($checkbox,$action,$value['am_name'],$company_name,$value['hr_name'],$client_category,$client_status,$value['address']);

                $data = array($action,$value['am_name'],$company_name,$value['hr_name'],$client_category,$client_status,$value['address']);
            }
            else{
                //$data = array($checkbox,$action,$value['am_name'],$company_name,$value['hr_name'],$client_status,$value['address']);

                $data = array($action,$value['am_name'],$company_name,$value['hr_name'],$client_status,$value['address']);
            }

            $clients[$i] = $data;
            $i++;
        }

        $json_data = array(
            'draw' => intval($draw),
            'recordsTotal' => intval($count),
            'recordsFiltered' => intval($count),
            "data" => $clients
        );

        echo json_encode($json_data);exit;
    }

    // Active client listing page function
    public function ActiveClient(){

        $utils = new Utils();
        $user =  \Auth::user();

        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isAccountant = $user_obj::isAccountant($role_id);
        $isManager = $user_obj::isManager($role_id);
        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE  /*|| $isAccountant*/){
            $client_array = ClientBasicinfo::getClientsByType(1,$user->id,$rolePermissions,1);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
        }
        else{
            $client_array = ClientBasicinfo::getClientsByType(0,$user->id,$rolePermissions,1);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
        }
        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($clients as $client){
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount'){
                $para_cat++;
            }
            else if($client['category'] == 'Moderate'){
                $mode_cat++;
            }
            else if($client['category'] == 'Standard'){
                $std_cat++;
            }
        }

        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $source = 'Active';
        return view('adminlte::client.clienttypeindex',compact('client_array','isManager','isSuperAdmin','count','active','passive','isBDE','para_cat','mode_cat','std_cat','source','account_manager','isAccountant','leaders','forbid','left'));
    }

    // Passive client listing page function
    public function PassiveClient(){

        $utils = new Utils();
        $user =  \Auth::user();

        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isAccountant = $user_obj::isAccountant($role_id);
        $isManager = $user_obj::isManager($role_id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE/* || $isAccountant*/){
            $client_array = ClientBasicinfo::getClientsByType(1,$user->id,$rolePermissions,0);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
        }
        else{
            $client_array = ClientBasicinfo::getClientsByType(0,$user->id,$rolePermissions,0);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
        }
        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($clients as $client){
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount'){
                $para_cat++;
            }
            else if($client['category'] == 'Moderate'){
                $mode_cat++;
            }
            else if($client['category'] == 'Standard'){
                $std_cat++;
            }
        }

        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $source = 'Passive';
        return view('adminlte::client.clienttypeindex',compact('client_array','isManager','isSuperAdmin','count','active','passive','isBDE','para_cat','mode_cat','std_cat','source','account_manager','isAccountant','leaders','forbid','left'));
    }

    // Leaders client listing page function
    public function LeadersClient(){

        $utils = new Utils();
        $user =  \Auth::user();

        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isAccountant = $user_obj::isAccountant($role_id);
        $isManager = $user_obj::isManager($role_id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE /*|| $isAccountant*/){
            $client_array = ClientBasicinfo::getClientsByType(1,$user->id,$rolePermissions,2);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
        }
        else{
            $client_array = ClientBasicinfo::getClientsByType(0,$user->id,$rolePermissions,2);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
        }
        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($clients as $client){
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount'){
                $para_cat++;
            }
            else if($client['category'] == 'Moderate'){
                $mode_cat++;
            }
            else if($client['category'] == 'Standard'){
                $std_cat++;
            }
        }

        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $source = 'Leaders';
        return view('adminlte::client.clienttypeindex',compact('client_array','isManager','isSuperAdmin','count','active','passive','isBDE','para_cat','mode_cat','std_cat','source','account_manager','isAccountant','leaders','forbid','left'));
    }

    // Forbid client listing page function
    public function ForbidClient(){

        $utils = new Utils();
        $user =  \Auth::user();

        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isAccountant = $user_obj::isAccountant($role_id);
        $isManager = $user_obj::isManager($role_id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE /*|| $isAccountant*/){
            $client_array = ClientBasicinfo::getClientsByType(1,$user->id,$rolePermissions,3);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
        }
        else{
            $client_array = ClientBasicinfo::getClientsByType(0,$user->id,$rolePermissions,3);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
        }
        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($clients as $client){
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount'){
                $para_cat++;
            }
            else if($client['category'] == 'Moderate'){
                $mode_cat++;
            }
            else if($client['category'] == 'Standard'){
                $std_cat++;
            }
        }

        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $source = 'Forbid';
        return view('adminlte::client.clienttypeindex',compact('client_array','isManager','isSuperAdmin','count','active','passive','isBDE','para_cat','mode_cat','std_cat','source','account_manager','isAccountant','leaders','forbid','left'));
    }

    // Left client listing page function
    public function LeftClient(){

        $utils = new Utils();
        $user =  \Auth::user();

        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isAccountant = $user_obj::isAccountant($role_id);
        $isManager = $user_obj::isManager($role_id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE /*|| $isAccountant*/){
            $client_array = ClientBasicinfo::getClientsByType(1,$user->id,$rolePermissions,4);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
        }
        else{
            $client_array = ClientBasicinfo::getClientsByType(0,$user->id,$rolePermissions,4);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
        }
        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($clients as $client){
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount'){
                $para_cat++;
            }
            else if($client['category'] == 'Moderate'){
                $mode_cat++;
            }
            else if($client['category'] == 'Standard'){
                $std_cat++;
            }
        }

        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $source = 'Left';
        return view('adminlte::client.clienttypeindex',compact('client_array','isManager','isSuperAdmin','count','active','passive','isBDE','para_cat','mode_cat','std_cat','source','account_manager','isAccountant','leaders','forbid','left'));
    }

    // Paramount client listing page function
    public function ParamountClient(){

        $utils = new Utils();
        $user =  \Auth::user();
        $category = 'Paramount';
        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isManager = $user_obj::isManager($role_id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE){
            $client_array = ClientBasicinfo::getClientsByType(1,$user->id,$rolePermissions,NULL,$category);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
        }
        else{
            $client_array = ClientBasicinfo::getClientsByType(0,$user->id,$rolePermissions,NULL,$category);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
        }
        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($clients as $client){
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount'){
                $para_cat++;
            }
            else if($client['category'] == 'Moderate'){
                $mode_cat++;
            }
            else if($client['category'] == 'Standard'){
                $std_cat++;
            }
        }

        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $source = 'Paramount';
        return view('adminlte::client.clienttypeindex',compact('client_array','isManager','isSuperAdmin','count','active','passive','isBDE','para_cat','mode_cat','std_cat','source','account_manager','leaders','forbid','left'));
    }

    // Moderate client listing page function
    public function ModerateClient(){

        $utils = new Utils();
        $user =  \Auth::user();
        $category = 'Moderate';
        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isManager = $user_obj::isManager($role_id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE){
            $client_array = ClientBasicinfo::getClientsByType(1,$user->id,$rolePermissions,NULL,$category);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
        }
        else{
            $client_array = ClientBasicinfo::getClientsByType(0,$user->id,$rolePermissions,NULL,$category);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
        }
        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($clients as $client){
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount'){
                $para_cat++;
            }
            else if($client['category'] == 'Moderate'){
                $mode_cat++;
            }
            else if($client['category'] == 'Standard'){
                $std_cat++;
            }
        }

        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $source = 'Moderate';
        return view('adminlte::client.clienttypeindex',compact('client_array','isManager','isSuperAdmin','count','active','passive','isBDE','para_cat','mode_cat','std_cat','source','account_manager','leaders','forbid','left'));
    }

    // Standard client listing page function
    public function StandardClient(){

        $utils = new Utils();
        $user =  \Auth::user();
        $category = 'Standard';
        // get logged in user company id
        $company_id = $user->company_id;

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isManager = $user_obj::isManager($role_id);

        $rolePermissions = \DB::table("permission_role")->where("permission_role.role_id",key($userRole))
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray();

        if($isSuperAdmin || $isManager || $isBDE){
            $client_array = ClientBasicinfo::getClientsByType(1,$user->id,$rolePermissions,NULL,$category);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(1,$user->id,$rolePermissions);
        }
        else{
            $client_array = ClientBasicinfo::getClientsByType(0,$user->id,$rolePermissions,NULL,$category);
            $count = sizeof($client_array);

            $clients = ClientBasicinfo::getAllClients(0,$user->id,$rolePermissions);
        }
        $i = 0;
        $active = 0;
        $passive = 0;
        $leaders = 0;
        $forbid = 0;
        $left = 0;
        $para_cat = 0;
        $mode_cat = 0;
        $std_cat = 0;
        foreach($clients as $client){
            if($client['status'] == 'Active' ){
                $active++;
            }
            else if ($client['status'] == 'Passive'){
                $passive++;
            }
            else if($client['status'] == 'Leaders' ){
                $leaders++;
            }
            else if($client['status'] == 'Forbid' ){
                $forbid++;
            }
            else if($client['status'] == 'Left' ){
                $left++;
            }

            if($client['category'] == 'Paramount'){
                $para_cat++;
            }
            else if($client['category'] == 'Moderate'){
                $mode_cat++;
            }
            else if($client['category'] == 'Standard'){
                $std_cat++;
            }
        }

        $account_manager=User::getAllUsers('recruiter','Yes');
        $account_manager[0] = 'Yet to Assign';

        $source = 'Standard';
        return view('adminlte::client.clienttypeindex',compact('client_array','isManager','isSuperAdmin','count','active','passive','isBDE','para_cat','mode_cat','std_cat','source','account_manager','leaders','forbid','left'));
    }

    public function create()
    {

        $co_prefix=ClientBasicinfo::getcoprefix();
        $co_category='';

        $client_cat=ClientBasicinfo::getCategory();
        $client_category='';

        $client_status_key=ClientBasicinfo::getStatus();
        $client_status = 1;

        $generate_lead = '1';
        $industry_res = Industry::orderBy('id','DESC')->get();
        $industry = array();

        $user = \Auth::user();
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $user_id = $user->id;

        // For account manager
        $users = User::getAllUsers('recruiter','Yes');
        $users[0] = 'Yet to Assign';

        if(sizeof($industry_res)>0){
            foreach($industry_res as $r){
                $industry[$r->id]=$r->name;
            }
        }

        // User Account Manager access check
        $user_acc_manager = \Auth::user()->account_manager;
        if ($user_acc_manager == 'No') {
            return view('errors.clientpermission');
        }

        $industry_id = '';
        $percentage_charged_below = '8.33';
        $percentage_charged_above = '8.33';

        $action = "add" ;
        return view('adminlte::client.create',compact('client_status','client_status_key','action','industry','users','isSuperAdmin','user_id','isAdmin','generate_lead','industry_id','co_prefix','co_category','client_cat','client_category','isBDE','percentage_charged_below','percentage_charged_above'));
    }

    public function postClientNames()
    {
        $client_ids = $_GET['client_ids'];

        $client_ids_array=explode(",",$client_ids);

        $client = ClientBasicinfo::getClientInfo($client_ids);

        echo json_encode($client);exit;

    }

    public function edit($id)
    {

        $generate_lead = '1';

        $co_prefix=ClientBasicinfo::getcoprefix();
        $client_cat=ClientBasicinfo::getCategory();
        $client_status_key=ClientBasicinfo::getStatus();

        $user = \Auth::user();
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isManager = $user_obj::isManager($role_id);
        $user_id = $user->id;

        $access_roles_id = array($isSuperAdmin,$isBDE,$isManager);

        $industry_res = Industry::orderBy('id','DESC')->get();
        $industry = array();

        if(sizeof($industry_res)>0){
            foreach($industry_res as $r){
                $industry[$r->id]=$r->name;
            }
        }

        $client = array();
        $client_basicinfo  = \DB::table('client_basicinfo')
            ->leftjoin('users', 'users.id', '=', 'client_basicinfo.account_manager_id')
            ->leftjoin('industry', 'industry.id', '=', 'client_basicinfo.industry_id')
            ->select('client_basicinfo.*', 'users.name as am_name','users.id as am_id','industry.name as ind_name')
            ->where('client_basicinfo.id','=',$id)
            ->get();

        foreach ($client_basicinfo as $key=>$value) {
            if(in_array($role_id,$access_roles_id) || ($value->am_id==$user_id)) {
                $client['name'] = $value->name;
                $client['display_name']=$value->display_name;
                $client['source'] = $value->source;
                //$client['fax'] = $value->fax;
                $client['mobile'] = $value->mobile;
                $client['other_number'] = $value->other_number;
                $client['am_name'] = $value->am_name;
                $client['mail'] = $value->mail;
                $client['s_email'] = $value->s_email;
                $client['ind_name'] = $value->ind_name;
                $client['website'] = $value->website;
                $client['description'] = $value->description;
                $client['gst_no'] = $value->gst_no;
                /*$client['tds'] = $value->tds;*/
                $client['coordinator_name'] = $value->coordinator_name;
                $co_category=$value->coordinator_prefix;
                $client['tan'] = $value->tan;
                $client['percentage_charged_below']=$value->percentage_charged_below;

                $client['percentage_charged_above']=$value->percentage_charged_above;

                $client_status=$value->status;

                $client_category=$value->category;
                /*echo $client_status;
                exit;*/
                $user_id = $value->account_manager_id;
                $industry_id = $value->industry_id;
                //$yet_to_assign_users_id = $value->yet_to_assign_user;
                $percentage_charged_below = $value->percentage_charged_below;
                $percentage_charged_above = $value->percentage_charged_above;
            }
            else {
                return view('errors.403');
            }
        }
        
        $client['id'] = $id;

        $client_address = \DB::table('client_address')->where('client_id','=',$id)->get();
        foreach ($client_address as $key=>$value) {
            $client['billing_country'] = $value->billing_country;
            $client['billing_state'] = $value->billing_state;
            $client['billing_street1'] = $value->billing_street1;
            $client['billing_street2'] = $value->billing_street2;
            $client['billing_code'] = $value->billing_code;
            $client['billing_city'] = $value->billing_city;
            $client['shipping_country'] = $value->shipping_country;
            $client['shipping_state'] = $value->shipping_state;
            $client['shipping_street1'] = $value->shipping_street1;
            $client['shipping_street2'] = $value->shipping_street2;
            $client['shipping_code'] = $value->shipping_code;
            $client['shipping_city'] = $value->shipping_city;
            $client['client_address_id'] = $value->id;
        }

         $client = (object)$client;
        // For account manager 
        $users = User::getAllUsers('recruiter','Yes');
        $users[0] = 'Yet to Assign';

        $yet_to_assign_users = User::getAllUsers('recruiter','Yes');
        $yet_to_assign_users[0] = '--Select User--';

        $action = "edit" ;
        return view('adminlte::client.edit',compact('client_status_key','action','industry','client','users','user_id','isSuperAdmin','isBDE','isAdmin','generate_lead','industry_id','co_prefix','co_category','client_status','client_cat','client_category','yet_to_assign_users','percentage_charged_below','percentage_charged_above'));
    }

    public function store(Request $request){

        $user_id = \Auth::user()->id;
        $user_name = \Auth::user()->name;
        $user_email = \Auth::user()->email;
        $input = $request->all();

        $client_basic_info = new ClientBasicinfo();
        $client_basic_info->name = $input['name'];
        $client_basic_info->display_name = $input['display_name'];
        $client_basic_info->mail = $input['mail'];
        $client_basic_info->s_email = $input['s_email'];
        $client_basic_info->description = $input['description'];
        $client_basic_info->mobile = $input['mobile'];
        $client_basic_info->other_number = $input['other_number'];
        $client_basic_info->website = $input['website'];

        if(isset($input['percentage_charged_below']) && $input['percentage_charged_below']!= '' ) {
            $client_basic_info->percentage_charged_below=$input['percentage_charged_below'];
        }
        else {
            $client_basic_info->percentage_charged_below='8.33';
        }
        
        if(isset($input['percentage_charged_above']) && $input['percentage_charged_above']!='' ) {
            $client_basic_info->percentage_charged_above=$input['percentage_charged_above'];
        }
        else {
             $client_basic_info->percentage_charged_above='8.33';
        }
        
        $status = $input['status'];
        $client_basic_info->status = $status;

        // save passive date for passive client
        if($status == '0') {
            $today_date = date('Y-m-d'); 
            $client_basic_info->passive_date = $today_date;
        }

        $client_basic_info->account_manager_id = $input['account_manager'];
        $client_basic_info->industry_id = $input['industry_id'];
        //$client_basic_info->source = $input['source'];
        $client_basic_info->about = $input['description'];
        if(isset($input['source']) && $input['source']!='')
            $client_basic_info->source = $input['source'];
        else
            $client_basic_info->source = '';
        if(isset($input['gst_no']) && $input['gst_no']!='')
            $client_basic_info->gst_no = $input['gst_no'];
        else
            $client_basic_info->gst_no = '';
        /*if(isset($input['tds']) && $input['tds']!='')
            $client_basic_info->tds = $input['tds'];
        else
            $client_basic_info->tds = '';*/
        if(isset($input['tan']) && $input['tan']!='')
            $client_basic_info->tan = $input['tan'];
        else
            $client_basic_info->tan = '';

        $client_basic_info->coordinator_name = $input['coordinator_name'];
        $client_basic_info->coordinator_prefix= $input['co_category'];

        if(isset($input['client_category'])) {
            $client_basic_info->category=$input['client_category'];
        }
        else {
            $client_basic_info->category='';
        }

        $client_basic_info->created_at = time();
        $client_basic_info->updated_at = time();

        if($client_basic_info->save()){

            $client_id = $client_basic_info->id;
            $client_name = $client_basic_info->name;

            $client_address = new ClientAddress();
            $client_address->client_id = $client_id;

            if(isset($input['billing_country']) && $input['billing_country']!=''){
                $client_address->billing_country = $input['billing_country'];
            }
            if(isset($input['billing_state']) && $input['billing_state']!=''){
                $client_address->billing_state = $input['billing_state'];
            }
            if(isset($input['billing_street1']) && $input['billing_street1']!=''){
                $client_address->billing_street1 = $input['billing_street1'];
            }
            if(isset($input['billing_street2']) && $input['billing_street2']!=''){
                $client_address->billing_street2 = $input['billing_street2'];
            }
            if(isset($input['billing_code']) && $input['billing_code']!=''){
                $client_address->billing_code = $input['billing_code'];
            }
            if(isset($input['billing_city']) && $input['billing_city']!=''){
                $client_address->billing_city = $input['billing_city'];
            }

            if(isset($input['shipping_country']) && $input['shipping_country']!=''){
                $client_address->shipping_country = $input['shipping_country'];
            }
            if(isset($input['shipping_state']) && $input['shipping_state']!=''){
                $client_address->shipping_state = $input['shipping_state'];
            }
            if(isset($input['shipping_street1']) && $input['shipping_street1']!=''){
                $client_address->shipping_street1 = $input['shipping_street1'];
            }
            if(isset($input['shipping_street2']) && $input['shipping_street2']!=''){
                $client_address->shipping_street2 = $input['shipping_street2'];
            }
            if(isset($input['shipping_code']) && $input['shipping_code']!=''){
                $client_address->shipping_code = $input['shipping_code'];
            }
            if(isset($input['shipping_city']) && $input['shipping_city']!=''){
                $client_address->shipping_city = $input['shipping_city'];
            }
            $client_address->updated_at = date("Y-m-d H:i:s");
            $client_address->save();

            // save client documents
            $client_contract = $request->file('client_contract');
            $client_logo = $request->file('client_logo');
            $others_doc = $request->file('others_doc');

            if (isset($client_contract) && $client_contract->isValid()) {
                $client_contract_name = $client_contract->getClientOriginalName();
                $filesize = filesize($client_contract);

                $dir_name = "uploads/clients/".$client_id."/";
                $client_contract_key = "uploads/clients/".$client_id."/".$client_contract_name;

                if (!file_exists($dir_name)) {
                    mkdir("uploads/clients/$client_id", 0777,true);
                }

                if(!$client_contract->move($dir_name, $client_contract_name)){
                    return false;
                }
                else{
                    $client_doc = new ClientDoc;

                    $client_doc->client_id = $client_id;
                    $client_doc->category = 'Client Contract';
                    $client_doc->name = $client_contract_name;
                    $client_doc->file = $client_contract_key;
                    $client_doc->uploaded_by = $user_id;
                    $client_doc->size = $filesize;
                    $client_doc->created_at = time();
                    $client_doc->updated_at = time();
                    $client_doc->save();
                }
            }

            if (isset($client_logo) && $client_logo->isValid()) {
                $client_logo_name = $client_logo->getClientOriginalName();
                $client_logo_filesize = filesize($client_logo);

                $dir_name = "uploads/clients/".$client_id."/";
                $client_logo_key = "uploads/clients/".$client_id."/".$client_logo_name;
                if (!file_exists($dir_name)) {
                    mkdir("uploads/clients/$client_id", 0777,true);
                }

                if(!$client_logo->move($dir_name, $client_logo_key)){
                    return false;
                }
                else{
                    $client_doc = new ClientDoc;

                    $client_doc->client_id = $client_id;
                    $client_doc->category = 'Client Logo';
                    $client_doc->name = $client_logo_name;
                    $client_doc->file = $client_logo_key;
                    $client_doc->uploaded_by = $user_id;
                    $client_doc->size = $client_logo_filesize;
                    $client_doc->created_at = time();
                    $client_doc->updated_at = time();
                    $client_doc->save();
                }
            }

            if (isset($others_doc) && $others_doc->isValid()) {
                $others_doc_name = $others_doc->getClientOriginalName();
                $others_filesize = filesize($others_doc);

                $dir_name = "uploads/clients/".$client_id."/";
                $others_doc_key = "uploads/clients/".$client_id."/".$others_doc_name;

                if (!file_exists($dir_name)) {
                    mkdir("uploads/clients/$client_id", 0777,true);
                }

                if(!$others_doc->move($dir_name, $others_doc_name)){
                    return false;
                }
                else{
                    $client_doc = new ClientDoc;

                    $client_doc->client_id = $client_id;
                    $client_doc->category = 'Others';
                    $client_doc->name = $others_doc_name;
                    $client_doc->file = $others_doc_key;
                    $client_doc->uploaded_by = $user_id;
                    $client_doc->size = $others_filesize;
                    $client_doc->created_at = time();
                    $client_doc->updated_at = time();
                    $client_doc->save();
                }
            }

            // TODO:: Notifications : On adding new client notify Super Admin via notification
            $module_id = $client_id;
            $module = 'Client';
            $message = $user_name . " added new Client";
            $link = route('client.show',$client_id);

            $super_admin_userid = getenv('SUPERADMINUSERID');
            $user_arr = array();
            $user_arr[] = $super_admin_userid;

            event(new NotificationEvent($module_id, $module, $message, $link, $user_arr));

            // Email Notification : data store in datebase
            $bdeuserid = getenv('BDEUSERID');
            $manageuserid = getenv('MANAGERUSERID');
            $superadminemail = User::getUserEmailById($super_admin_userid);
            $bdeemail = User::getUserEmailById($bdeuserid);
            $manageemail = User::getUserEmailById($manageuserid);
            $cc_users_array = array($superadminemail,$bdeemail,$manageemail);

            $module = "Client";
            $sender_name = $user_id;
            $to = $user_email;
            $subject = "New Client - " . $client_name . " - " . $input['billing_city'];
            $message = "<tr><td>" . $user_name . " added new Client </td></tr>";
            $module_id = $client_id;
            $cc = implode(",",$cc_users_array);

            event(new NotificationMail($module,$sender_name,$to,$subject,$message,$module_id,$cc));

            return redirect()->route('client.index')->with('success','Client Created Successfully');
        }
        else{
            return redirect('client/create')->withInput(Input::all())->withErrors($client_basic_info->errors());
        }
    }

    public function show($id)
    {

        $user = \Auth::user();
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isAdmin = $user_obj::isAdmin($role_id);
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isAccountant = $user_obj::isAccountant($role_id);
        $isManager = $user_obj::isManager($role_id);
        $user_id = $user->id;

        $access_roles_id = array($isManager,$isSuperAdmin,$isBDE/*,$isAccountant*/);

        $client_basicinfo_model = new ClientBasicinfo();
        $client_upload_type = $client_basicinfo_model->client_upload_type;

        $client = array();
        $client_basicinfo  = \DB::table('client_basicinfo')
            ->leftjoin('users', 'users.id', '=', 'client_basicinfo.account_manager_id')
            ->leftjoin('industry', 'industry.id', '=', 'client_basicinfo.industry_id')
            ->select('client_basicinfo.*', 'users.name as am_name','users.id as am_id', 'industry.name as ind_name')
            ->where('client_basicinfo.id','=',$id)
            ->get();

        $client['id'] = $id;
   
        foreach ($client_basicinfo as $key=>$value) {
            $client_category = $value->category;
            if ($client_category == 'Moderate' || $client_category == 'Standard') {
                $manager_user_id = env('MANAGERUSERID');
            }
            else {
                $manager_user_id = 0;
            }

            if(in_array($role_id,$access_roles_id) || ($value->am_id==$user_id) /*|| ($manager_user_id == $user_id)*/) {
                $client['name'] = $value->name;
                $client['source'] = $value->source;
                $client['fax'] = $value->fax;
                $client['mobile'] = $value->mobile;
                $client['am_name'] = $value->am_name;
                $client['mail'] = $value->mail;
                $client['ind_name'] = $value->ind_name;
                $client['website'] = $value->website;
                $client['description'] = $value->description;
                $client['gst_no'] = $value->gst_no;
                /*$client['tds'] = $value->tds;*/
                $client['coordinator_name'] = $value->coordinator_prefix. " " .$value->coordinator_name;
                $client['tan'] = $value->tan;
                $client['status']=$value->status;
                $client['category']=$value->category;
                $client['display_name'] = $value->display_name;

                if(isset($client['status'])) {
                    if($client['status'] == '1') {
                        $client['status']='Active';
                    }
                    else if($client['status'] == '0') {
                        $client['status']='Passive';
                    }
                    else if($client['status'] == '2') {
                        $client['status']='Leaders';
                    }
                    else if($client['status'] == '3') {
                        $client['status']='Forbid';
                    }
                    else if($client['status'] == '4') {
                        $client['status']='Left';
                    }
                }

                $client['percentage_charged_below']=$value->percentage_charged_below;
                $client['percentage_charged_above']=$value->percentage_charged_above;

                if($value->am_id==$user->id) {
                    $client['client_owner'] = true;
                }
                else {
                    $client['client_owner'] = false;
                }
            }
           else {
               return view('errors.403');
           }
        }

        $client_address = \DB::table('client_address')->where('client_id','=',$id)->get();

        foreach ($client_address as $key=>$value) {
            $client['billing_country'] = $value->billing_country;
            $client['billing_state'] = $value->billing_state;
            $client['billing_street'] = $value->billing_street1."\n".$value->billing_street2;
            $client['billing_code'] = $value->billing_code;
            $client['billing_city'] = $value->billing_city;
            $client['shipping_country'] = $value->shipping_country;
            $client['shipping_state'] = $value->shipping_state;
            $client['shipping_street'] = $value->shipping_street1."\n".$value->shipping_street2;
            $client['shipping_code'] = $value->shipping_code;
            $client['shipping_city'] = $value->shipping_city;
        }

        $i = 0;
        $client['doc'] = array();
        $client_doc = \DB::table('client_doc')
                    ->join('users', 'users.id', '=', 'client_doc.uploaded_by')
                    ->select('client_doc.*', 'users.name as upload_name')
                    ->where('client_id','=',$id)
                    ->get();

        $utils = new Utils();
        foreach ($client_doc as $key=>$value) {
            $client['doc'][$i]['name'] = $value->name ;
            $client['doc'][$i]['id'] = $value->id;
            $client['doc'][$i]['url'] = "../".$value->file ;//$utils->getUrlAttribute($value->file);
            $client['doc'][$i]['category'] = $value->category ;
            $client['doc'][$i]['uploaded_by'] = $value->upload_name ;
            $client['doc'][$i]['size'] = $utils->formatSizeUnits($value->size);
            $i++;
            if(array_search($value->category, $client_upload_type))  {
                unset($client_upload_type[array_search($value->category, $client_upload_type)]);
            }
        }

        $client_upload_type['Others'] = 'Others';
    
        return view('adminlte::client.show',compact('client','client_upload_type','isSuperAdmin','isAdmin','isStrategy','isManager','isBDE'));
    }

    public function attachmentsDestroy($docid){

        $client_attach=\DB::table('client_doc')->select('client_doc.*')->where('id','=',$docid)->first();

        if(isset($client_attach)) {
            $path="uploads/clients/".$client_attach->client_id . "/" . $client_attach->name;

            unlink($path);

            $clientid=$client_attach->client_id;
    
            $client_doc=ClientDoc::where('id','=',$docid)->delete();
        }

        return redirect()->route('client.show',[$clientid])->with('success','Attachment deleted Successfully');
    }

    public function upload(Request $request){
        $client_upload_type = $request->client_upload_type;
        $file = $request->file('file');
        $client_id = $request->id;
        $user_id = \Auth::user()->id;

        if (isset($file) && $file->isValid()) {
            $doc_name = $file->getClientOriginalName();
            $doc_filesize = filesize($file);

            $dir_name = "uploads/clients/".$client_id."/";
            $others_doc_key = "uploads/clients/".$client_id."/".$doc_name;

            if (!file_exists($dir_name)) {
                mkdir("uploads/clients/$client_id", 0777,true);
            }

            if(!$file->move($dir_name, $doc_name)){
                return false;
            }
            else{
                $client_doc = new ClientDoc;

                $client_doc->client_id = $client_id;
                $client_doc->category = $client_upload_type;
                $client_doc->name = $doc_name;
                $client_doc->file = $others_doc_key;
                $client_doc->uploaded_by = $user_id;
                $client_doc->size = $doc_filesize;
                $client_doc->created_at = time();
                $client_doc->updated_at = time();
                $client_doc->save();
            }
        }

        return redirect()->route('client.show',[$client_id])->with('success','Attachment uploaded successfully');
    }

    public function delete($id){

        $lead_res = \DB::table('client_basicinfo')
                    ->select('client_basicinfo.lead_id')
                    ->where('id','=',$id)
                    ->first();

        if (isset($lead_res) && $lead_res !='') {
            $lead_id = $lead_res->lead_id;
        }

        if (isset($lead_id) && $lead_id !='') {
            $lead = Lead::find($lead_id);
            if (isset($lead) && $lead != '') {
                $lead->convert_client = 0;
                $lead->save();
            }
        }
        $res = ClientBasicinfo::checkAssociation($id);

        if($res){
            // delete address info
            \DB::table('client_address')->where('client_id', '=', $id)->delete();

            // delete attachments
            \DB::table('client_doc')->where('client_id', '=', $id)->delete();

            // delete basic info
            ClientBasicinfo::where('id',$id)->delete();

            // unlink documents
            $dir_name = "uploads/clients/".$id."/";
            $client_doc = new ClientDoc();
            if(is_dir($dir_name)){
                $response = $client_doc->recursiveRemoveDirectory($dir_name);
            }

            return redirect()->route('client.index')->with('success','Client deleted Successfully');
        }else{
            return redirect()->route('client.index')->with('error','Client is associated with job.!!');
        }
        return redirect()->route('client.index'); 
    }

    public function update(Request $request, $id){
        $user_id = \Auth::user()->id;

        $input = $request->all();
        $input = (object)$input;

        $client_basicinfo = ClientBasicinfo::find($id);

        $client_basicinfo->name = $input->name;
        $client_basicinfo->display_name = $input->display_name;
        $client_basicinfo->mobile = $input->mobile;
        $client_basicinfo->other_number = $input->other_number;
        $client_basicinfo->mail = $input->mail;
        $client_basicinfo->s_email = $input->s_email;
        $client_basicinfo->description = $input->description;

        if(isset($input->percentage_charged_below) && $input->percentage_charged_below!= '' ) {
            $client_basicinfo->percentage_charged_below=$input->percentage_charged_below;
        }
        else if (isset($client_basicinfo->percentage_charged_below) && $client_basicinfo->percentage_charged_below != '') {
            $client_basicinfo->percentage_charged_below = $client_basicinfo->percentage_charged_below;
        }
        else {
            $client_basicinfo->percentage_charged_below='8.33';
        }
        
        if(isset($input->percentage_charged_above) && $input->percentage_charged_above!='' ) {
            $client_basicinfo->percentage_charged_above=$input->percentage_charged_above;
        }
        else if (isset($client_basicinfo->percentage_charged_above) && $client_basicinfo->percentage_charged_above != '') {
            $client_basicinfo->percentage_charged_above = $client_basicinfo->percentage_charged_above;
        }
        else {
             $client_basicinfo->percentage_charged_above='8.33';
        }
        
        //$client_basicinfo->fax = $input->fax;
        $client_basicinfo->industry_id = $input->industry_id;

        $status=$input->status;
        $client_basicinfo->status = $status;

        // save passive date for passive client
        if($status == '0') {
            $today_date = date('Y-m-d'); 
            $client_basicinfo->passive_date = $today_date;
        }

        $client_basicinfo->website = $input->website;
        if(isset($input->source) && $input->source!=''){
            $client_basicinfo->source = $input->source;
        }
        else{
            $client_basicinfo->source = '';
        }

        //$client_basicinfo->gst_no = $input->gst_no;
        //$client_basicinfo->tds = $input->tds;
        $client_basicinfo->coordinator_name = $input->coordinator_name;

        $client_basicinfo->coordinator_prefix=$input->co_category;
        $client_basicinfo->account_manager_id = $input->account_manager;

        if(isset($input->gst_no) && $input->gst_no!='')
            $client_basicinfo->gst_no = $input->gst_no;
        else
            $client_basicinfo->gst_no = '';

       /* if(isset($input->tds) && $input->tds!='')
            $client_basicinfo->tds = $input->tds;
        else
            $client_basicinfo->tds = '';*/
        
        if(isset($input->tan) && $input->tan!='')
            $client_basicinfo->tan = $input->tan;
        else
            $client_basicinfo->tan = '';

        if(isset($input->client_category)) {
            $client_basicinfo->category=$input->client_category;
        }
        else if (isset($client_basicinfo->category) && $client_basicinfo->category != '') {
            $client_basicinfo->category = $client_basicinfo->category;
        }
        else {
            $client_basicinfo->category='';
        }
        
        if($client_basicinfo->save()){

            // update client address
            $client_address = ClientAddress::find($input->client_address_id);
               if(!isset($client_address) && empty($client_address)){
                   $client_address = new ClientAddress();
                   $client_address->client_id = $id;
               }
            if(isset($input->billing_country) && $input->billing_country!=''){
                $client_address->billing_country = $input->billing_country;
            }
            if(isset($input->billing_state) && $input->billing_state!=''){
                $client_address->billing_state = $input->billing_state;
            }
            if(isset($input->billing_street1) && $input->billing_street1!=''){
                $client_address->billing_street1 = $input->billing_street1;
            }
            if(isset($input->billing_street2) && $input->billing_street2!=''){
                $client_address->billing_street2 = $input->billing_street2;
            }
            if(isset($input->billing_code) && $input->billing_code!=''){
                $client_address->billing_code = $input->billing_code;
            }
            if(isset($input->billing_city) && $input->billing_city!=''){
                $client_address->billing_city = $input->billing_city;
            }

            if(isset($input->shipping_country) && $input->shipping_country!=''){
                $client_address->shipping_country = $input->shipping_country;
            }
            if(isset($input->shipping_state) && $input->shipping_state!=''){
                $client_address->shipping_state = $input->shipping_state;
            }
            if(isset($input->shipping_street1) && $input->shipping_street1!=''){
                $client_address->shipping_street1 = $input->shipping_street1;
            }
            if(isset($input->shipping_street2) && $input->shipping_street2!=''){
                $client_address->shipping_street2 = $input->shipping_street2;
            }
            if(isset($input->shipping_code) && $input->shipping_code!=''){
                $client_address->shipping_code = $input->shipping_code;
            }
            if(isset($input->shipping_city) && $input->shipping_city!=''){
                $client_address->shipping_city = $input->shipping_city;
            }
            $client_address->updated_at = date("Y-m-d H:i:s");
            $client_address->save();

            // if account manager change then jobs hiring manager all changes
            $job_ids = JobOpen::getJobIdByClientId($id);
            if ($input->account_manager == '0') {
                $super_admin_userid = getenv('SUPERADMINUSERID');
                $a_m = $super_admin_userid;
            }
            else {
                $a_m = $input->account_manager;
            }
            foreach ($job_ids as $key => $value) {

                \DB::statement("UPDATE job_openings SET hiring_manager_id = '$a_m' where id=$value");

                $check_job_user_id = JobVisibleUsers::getCheckJobUserIdAdded($value,$a_m);

                if ($check_job_user_id == false) {
                    $job_visible_users = new JobVisibleUsers();
                    $job_visible_users->job_id = $value;
                    $job_visible_users->user_id = $a_m;
                    $job_visible_users->save();
                }
            }

            return redirect()->route('client.index')->with('success','Client updated successfully');
        }else{
            return redirect('client/'.$client_basicinfo->id.'/edit')->withInput(Input::all())->withErrors ( $client_basicinfo->errors() );
        }
    }
   
    public function postClientEmails()
    {
        $user =  \Auth::user();

        $user_id=$user->id;

        $client_ids = $_POST['client_ids'];

        $client_ids_array=explode(",",$client_ids);

        foreach($client_ids_array as $key => $value) {
            $client_email=ClientBasicinfo::getClientEmailByID($value);

            $client_name=ClientBasicinfo::getClientNameByID($value);

            $client_company=ClientBasicinfo::getCompanyOfClientByID($value);            

            $message="Open Position List - $client_company";
            
        
            /*$email_notification= new EmailsNotifications();
            $email_notification->module='Client';
            $email_notification->sender_name=$user_id;
            $email_notification->to=$client_email;
            $email_notification->cc='meet@trajinfotech.com';
            $email_notification->bcc="";
            $email_notification->subject=$message;
            $email_notification->message="";
            $email_notification->sent_date=date("Y-m-d H:i:s");
            $email_notification->status='0';
            $email_notification->module_id=$value;*/

            $module='Client';
            $sender_name=$user_id;
            $to=$client_email;
           // $cc=$client_email;
            $cc='meet@trajinfotech.com';
            $subject=$message;
            $body_message="Dear $client_name, <br><br>Greetings From $client_company! <br><br> Request You to Kindly Advise on the list of Priority Positions for this Week to Focus Upon Accordingly.<br><br>Awaiting your revert to expedite accordingly.<br><br>Thanks.";
           
            $module_id=$value;
            
            event(new NotificationMail($module,$sender_name,$to,$subject,$body_message,$module_id,$cc));
            
        }

        return redirect()->route('client.index')->with('success','Successfully');
    }

    public function getMonthWiseClient()
    {

        $user =  \Auth::user();

        // get role of logged in user
        $userRole = $user->roles->pluck('id','id')->toArray();

        $role_id = key($userRole);

        $user_obj = new User();

        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);
        $isBDE = $user_obj::isBDE($role_id);
        $isManager = $user_obj::isManager($role_id);

        if($isSuperAdmin || $isBDE || $isManager){
            $response = ClientBasicinfo::getMonthWiseClientByUserId($user->id,1);
            $count = sizeof($response);
        }
        else{
            $response = ClientBasicinfo::getMonthWiseClientByUserId($user->id,0);
            $count = sizeof($response);
        }

        return view('adminlte::client.monthwiseclient', array('clients' => $response,'count' => $count),compact('isSuperAdmin','isBDE','isManager'));
    }

    public function getAccountManager(Request $request)
    {
        $account_manager=$request->get('account_manager');
        $id = $request->get('id');

        $act_man=ClientBasicinfo::find($id);

        $a_m='';

        if(isset($act_man)){
            $a_m=$account_manager;
        }

        $act_man->account_manager_id=$a_m;
        $act_man->save();

        if ($act_man) {
            $job_ids = JobOpen::getJobIdByClientId($id);
            if ($account_manager == '0') {
                $super_admin_userid = getenv('SUPERADMINUSERID');
                $a_m = $super_admin_userid;
            }
            foreach ($job_ids as $key => $value) {

                \DB::statement("UPDATE job_openings SET hiring_manager_id = '$a_m' where id=$value");

                $check_job_user_id = JobVisibleUsers::getCheckJobUserIdAdded($value,$a_m);

                if ($check_job_user_id == false) {
                    $job_visible_users = new JobVisibleUsers();
                    $job_visible_users->job_id = $value;
                    $job_visible_users->user_id = $a_m;
                    $job_visible_users->save();
                }
            }
        }

       return redirect()->route('client.index')->with('success', 'Client Account Manager Updated successfully');
    }
    public function importExport(){

        return view('adminlte::client.import');
    }

    public function importExcel(Request $request)
    {
        if($request->hasFile('import_file'))
        {
            $path = $request->file('import_file')->getRealPath();

            $data = Excel::load($path, function ($reader) {})->get();

            $messages = array();

            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $key => $value) {
                    if(!empty($value)) {
                        // $sr_no = $value['sr_no'];
                        $company_name = $value['company_name'];
                        $hrcoordinator_prefix = $value['hr_coordinator_prefix'];
                        $hrcoordinator_name = $value['hr_coordinator_name'];
                        $display_name = $value['display_namemax_7_characters'];
                        $category = $value['select_categoryparamountmoderatestandard'];
                        $mobile_number = $value['mobile_number'];
                        $email_id = $value['email_id'];
                        $other_number = $value['other_number_optional'];
                        $secondary_email = $value['secondary_emailoptional'];
                        $account_manager = $value['account_manager'];
                        $website = $value['website'];
                        $industry = $value['industry'];
                        $tan = $value['tan'];
                        $source = $value['source'];
                        $gst = $value['gst_number'];
                        $about = $value['about_client'];
                        $below_am_postition = $value['charged_below_am_position'];
                        $above_am_position = $value['charged_above_am_position'];
                        $status = $value['statusactive_passive'];

                        // For address
                        $address = $value['address'];
                        $city = $value['city'];
                        $state = $value['state'];
                        $pincode = $value['pin_code'];
                        $country = $value['country'];
                        
                        // first check email already exist or not , if exist doesnot update data
                        if (isset($company_name) && $company_name != '') {
                            $client_cnt = ClientBasicinfo::checkClientByEmail($email_id);

                            if($client_cnt>0) {
                                $messages[] = "Record $company_name already present.";
                            }
                            else {
                                // get user id from managed_by (i.e. username)
                                if ($account_manager == 'No') {
                                    $acc_mngr_id = 0;
                                }
                                else {
                                    $acc_mngr_id = User::getUserIdByName($account_manager);
                                }

                                // if($acc_mngr_id>0) {
                                    // Insert new client
                                    $client_basic_info = new ClientBasicinfo();
                                    $client_basic_info->name = $company_name;
                                    $client_basic_info->mail = $email_id;
                                    $client_basic_info->mobile = $mobile_number;
                                    $client_basic_info->other_number = $other_number;
                                    $client_basic_info->account_manager_id = $acc_mngr_id;

                                    $industry_id = Industry::getIndustryIdByName($industry);
                                    if ($industry_id == '0') {
                                        $industry_id = 60;
                                    }
                                    $client_basic_info->industry_id = $industry_id;
                                    // print_r($industry_id);exit;

                                    $client_basic_info->website = $website;
                                    $client_basic_info->source = $source;
                                    $client_basic_info->about = $about;
                                    $client_basic_info->gst_no = $gst;
                                    $client_basic_info->coordinator_name = $hrcoordinator_name;
                                    $client_basic_info->tan = $tan;
                                    $client_basic_info->display_name = $display_name;
                                    $client_basic_info->s_email = $secondary_email;
                                    $client_basic_info->percentage_charged_above = $above_am_position;
                                    $client_basic_info->percentage_charged_below = $below_am_postition;
                                    if ($status == 'Active' || $status == 'Active ' || $status == 'active') {
                                        $status_id = 1;
                                        $client_basic_info->status = $status_id;
                                    }
                                    else if ($status == 'Passive' || $status == 'Passive ' || $status == 'passive') {
                                        $status_id = 0;
                                        $client_basic_info->status = $status_id;
                                    }
                                    $client_basic_info->coordinator_prefix = $hrcoordinator_prefix;
                                    $client_basic_info->category = $category;

                                    if($client_basic_info->save()) {
                                        $client_id = $client_basic_info->id;

                                        $input['client_id'] = $client_id;

                                        // billing address
                                        $input['billing_country'] = $country;
                                        $input['billing_state'] = $state;
                                        $input['billing_city'] = $city;
                                        $input['billing_code'] = $pincode;
                                        $input['billing_street1'] = $address;

                                        // shipping address
                                        $input['shipping_country'] = $country;
                                        $input['shipping_state'] = $state;
                                        $input['shipping_city'] = $city;
                                        $input['shipping_code'] = $pincode;
                                        $input['shipping_street1'] = $address;

                                        ClientAddress::create($input);
                                        if ($client_id > 0) {
                                            $messages[] = "Record $company_name inserted Successfully.";
                                        }

                                    }
                                    else {
                                        $messages[] = "Error while inserting Record $company_name.";
                                    }
                                // }
                                // else {
                                //     $messages[] = "Error while inserting Record $company_name.";
                                // }
                            }
                        }
                    }
                    else {
                        $messages[] = "No Data in file";
                    }
                }
            }
            return view('adminlte::client.import',compact('messages'));
        }
    }

    public function remarks($id){
        $user =  \Auth::user();
        $userRole = $user->roles->pluck('id','id')->toArray();
        $role_id = key($userRole);

        $user_obj = new User();
        $isSuperAdmin = $user_obj::isSuperAdmin($role_id);

       $user_id = \Auth::user()->id;
       $client_id = $id;

       $client = ClientBasicinfo::find($client_id);

       $post = $client->post()->orderBy('created_at', 'desc')->get();
//echo $isSuperAdmin;exit;
       return view('adminlte::client.remarks',compact('user_id','client_id','post','client','isSuperAdmin'));

    }

    public function writePost(Request $request, $client_id){

        $input = $request->all();

        $user_id = $input['user_id'];
        $client_id = $input['client_id'];
        $content = $input['content'];

        if(isset($user_id) && $user_id>0){

            $post = new Post();
            $post->content = $content;
            $post->user_id = $user_id;
            $post->client_id = $client_id;
            $post->approved = 0;
            $post->approved_by = 0;
            $post->created_at = time();
            $post->updated_at = time();
            $post->save();
        }

        return redirect()->route('client.remarks',[$client_id]);
    }


    public function writeComment(Request $request,$post_id){
        $input = $request->all();

        $client_id = $input['client_id'];

        $user_id = \Auth::user()->id;
        $currentUser = User::find($user_id);
        $post = Post::find($post_id);

        $comment = $post->comment([
            "title" => $input["content"],
            "body" => $input["content"]
        ], $currentUser);

        $returnValue["success"] = true;
        $returnValue["message"] = "Commment recorded";
        $returnValue["id"] = $comment->id;

        return redirect()->route('client.remarks',[$client_id]);
    }

    public function updateClientRemarks(Request $request, $client_id,$post_id){

        $input = $request->all();

        $user_id = $input['user_id'];
        $client_id = $input['client_id'];

        $response = Post::updatePost($post_id,$input["content"]);
        $returnValue["success"] = true;
        $returnValue["message"] = "Remarks Updated";
        $returnValue["id"] = $post_id;

       return redirect()->route('client.remarks',[$client_id]);

    }

    public function updateComment(){

        $id = $_POST['id'];
        $content = $_POST['content'];

        $response['returnvalue'] = 'invalid';

        $res = Comments::updateComment($id,$content);
//exit;
        if($res){
            $response['returnvalue'] = 'valid';
        }
        return json_encode($response);exit;
    }

    public function commentDestroy($id){
        $response['returnvalue'] = 'invalid';
        $res = Comments::deleteComment($id);
        if($res){
            // delete replies on it
            //Comments::where('parent_id', '=', $id)->delete();
            $response['returnvalue'] = 'valid';
        }

        return json_encode($response);exit;
    }

    public function postDestroy($id){

        $response['returnvalue'] = 'invalid';
        $res = Post::deletePost($id);
        if($res){
            \DB::table('comments')->where('commentable_id', '=', $id)->delete();
            $response['returnvalue'] = 'valid';
        }

        return json_encode($response);exit;

    }

}
