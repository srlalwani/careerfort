<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    public $table = "industry";
    public $timestamps = false;

    public static function getIndustryIdByName($name){
        $industry_id = 0;

        $query = Industry::query();
        $query = $query->where('name','like',"%$name%");
        $query = $query->first();

        if(isset($query)){
            $industry_id = $query->id;
        }
        return $industry_id;
    }
}
