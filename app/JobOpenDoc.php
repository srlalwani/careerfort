<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOpenDoc extends Model
{
    public $table = "job_openings_doc";
}
