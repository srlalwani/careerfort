<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingDoc extends Model
{
    public $table = "training_doc";
}
