<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateSource extends Model
{
    //
    public $table = "candidate_source";

    public static $rules = array(
        'name' => 'required',
    );

    public function messages()
    {
        return [
            'name.required' => 'Name is required field',
        ];
    }

    public static function getSourceIdByName($name){
        $source_id = 0;

        $query = CandidateSource::query();
        $query = $query->where('name','like',$name);
        $query = $query->first();

        if(isset($query)){
            $source_id = $query->id;
        }
        return $source_id;
    }
}
